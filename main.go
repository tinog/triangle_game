package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
	"triangle_game/board"
)

func symmetric4() {
	b1 := board.NewBoard(4, 0, 0)
	b2 := board.NewBoard(4, 1, 0)
	b5 := board.NewBoard(4, 2, 1)

	b1Games := b1.GetAllWinGames()
	b2Games := b2.GetAllWinGames()
	b5Games := b5.GetAllWinGames()

	bGames := append(b1Games, b2Games...)
	bGames = append(bGames, b5Games...)
	for _, bGame := range bGames {
		jumps := bGame.BoardToMoves()
		fmt.Print(len(jumps), ": ")
		for _, jump := range jumps {
			fmt.Print(jump + " ")
		}
		fmt.Print("\n")
	}
}

func allSolutions(n int) {
	solutions := make([]board.Board, 0)
	for x := 0; x < n; x++ {
		for y := 0; y <= x; y++ {
			b := board.NewBoard(n, x, y)
			solutions = append(solutions, b.GetAllWinGames()...)
		}
	}

	for _, solution := range solutions {
		solution.PrintMoves()
	}
}

func aSolution(n int) {
	rand.Seed(time.Now().UTC().UnixNano())
	for {
		x := rand.Intn(n)
		y := rand.Intn(x + 1)
		b := board.NewBoard(n, x, y)
		winGame, foundWinGame, _ := b.DashForWinGame(0, 0, 1000)
		if foundWinGame {
			winGame.PrintMoves()
			return
		}
	}
}

func main() {
	fmt.Println("Executing triangle game")
	// board.RunAllTests()
	if len(os.Args) != 3 {
		fmt.Println("Pass arguments n and (one, all or upto)")
		return
	}

	n, err := strconv.Atoi(os.Args[1])
	if err != nil {
		fmt.Println(err.Error())
	}

	exhaustive := os.Args[2]
	if exhaustive == "one" {
		if n > 0 {
			aSolution(n)
		}

	} else if exhaustive == "all" {
		allSolutions(n)
	} else if exhaustive == "upto" {
		for i := 4; i <= n; i++ {
			aSolution(i)
		}
	} else {
		fmt.Println("Pass arguments n and (one or all)")
		return
	}

}

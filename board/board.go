package board

import (
	"fmt"
	"regexp"
	"strconv"
)

/* @el X, Y coordinates of stone
 * @el DX, DY direction of jump (DX, DY) element of (-1, 0, 1)
 */
type Jump struct {
	X, Y   int
	DX, DY int
}

type Board struct {
	data         [][]bool
	Jumps        []string
	StartRemoved int
}

/* Converts 2d-position to 1d-position (enumeration)
 * @pre x >= y
 */
func ConvertPos(x, y int) int {
	x_s := x * x
	return (x_s+x+1)/2 + y + 1
}

/* Creates a Board
 * @arg n length of triangle
 * @arg x, y removing the stone x, y
 * @ret Board of all true values except at x,y
 * @pre n > x >= y
 */
func NewBoard(n, x, y int) Board {
	if x >= n || y > x {
		panic("Newboard: Precondition n > x >= y not met!")
	}

	data := make([][]bool, n)
	for row := range data {
		data[row] = make([]bool, row+1)
		for el := range data[row] {
			data[row][el] = true
		}
	}

	data[x][y] = false
	b := Board{data, make([]string, 0), ConvertPos(x, y)}
	return b
}

func (b Board) PrintMoves() {
	jumps := b.BoardToMoves()
	fmt.Print(b.n(), ": ")
	for _, jump := range jumps {
		fmt.Print(jump + " ")
	}
	fmt.Print("\n")
}

// Converts the Jumps []string to Moves []string (merges 1-4 4-6 to 1-4-6)
func (b Board) BoardToMoves() []string {
	re := regexp.MustCompile("(\\d+)")
	reLatter := regexp.MustCompile("-(\\d+)")
	moves := make([]string, 0)
	current := make([]string, 0)
	jumps := b.Jumps
	for _, jump := range jumps {
		if len(current) == 0 {
			current = re.FindAllString(jump, -1)
		} else {
			matchingNumber := re.FindString(jump)
			if matchingNumber == current[len(current)-1] {
				submatch := reLatter.FindStringSubmatch(jump)
				current = append(current, submatch[1])
			} else {
				current_str := ""
				for _, str := range current {
					current_str += str + "-"
				}
				current_str = current_str[:len(current_str)-1]
				moves = append(moves, current_str)
				current = re.FindAllString(jump, -1)
			}
		}
	}

	if len(current) != 0 {
		currentStr := ""
		for _, str := range current {
			currentStr += str + "-"
		}
		currentStr = currentStr[:len(currentStr)-1]
		moves = append(moves, currentStr)
	}

	return moves
}

func (b Board) getAllJumps() []Board {
	allJumps := make([]Board, 0)
	n := b.n()
	for x := 0; x < n; x++ {
		for y := 0; y <= x; y++ {
			allJumps = append(allJumps, b.getJumpsOf(x, y)...)
		}
	}
	return allJumps
}

// Gets all single jumps that a single stone on the board can do
func (b Board) getJumpsOf(x, y int) []Board {
	allJumps := make([]Board, 0)
	jumpArray := [6]Jump{
		{x, y, -1, -1},
		{x, y, 1, 1},
		{x, y, 1, 0},
		{x, y, -1, 0},
		{x, y, 0, 1},
		{x, y, 0, -1},
	}
	for _, jump := range jumpArray {
		if b.isValidJump(jump) {
			allJumps = append(allJumps, b.commitJump(jump))
		}
	}
	// Calls isValidJump repeatedly
	// Might call commitJump repeatedly
	return allJumps
}

// Commit a jump (change data and append Jumps string)
// Panic when jump not legal
func (b Board) commitJump(jump Jump) Board {
	if !b.isValidJump(jump) {
		panic("Tried to commit non-valid jump")
	}
	bNew := b.dublicate()
	data := bNew.data
	x, y, dx, dy := jump.X, jump.Y, jump.DX, jump.DY
	data[x][y] = false
	data[x+dx][y+dy] = false
	data[x+2*dx][y+2*dy] = true
	fromStr := strconv.Itoa(ConvertPos(x, y))
	toStr := strconv.Itoa(ConvertPos(x+2*dx, y+2*dy))
	bNew.Jumps = append(bNew.Jumps, fromStr+"-"+toStr)
	return bNew
}

func (b Board) isValidJump(jump Jump) bool {
	data := b.data
	x, y, dx, dy := jump.X, jump.Y, jump.DX, jump.DY
	// Check that start and end point are on board
	if !b.isOnBoard(x, y) || !b.isOnBoard(x+2*dx, y+2*dy) {
		return false
	}
	// Check that start and middle are occupied, but landing free
	return data[x][y] && data[x+dx][y+dy] && !data[x+2*dx][y+2*dy]
}

// Dublicates a board without changing anything
func (b Board) dublicate() Board {
	srcData := b.data
	dstData := make([][]bool, len(srcData))
	for row := range dstData {
		dstData[row] = make([]bool, len(srcData[row]))
		copy(dstData[row], srcData[row])
	}

	srcJumps := b.Jumps
	dstJumps := make([]string, len(srcJumps))
	copy(dstJumps, srcJumps)

	return Board{dstData, dstJumps, b.StartRemoved}
}

// Returns if x, y on board
func (b Board) isOnBoard(x, y int) bool {
	return b.n() > x && x >= y && y >= 0
}

// Gets length of board
func (b Board) n() int {
	return len(b.data)
}

func (b Board) stones() int {
	n := b.n()
	count := 0
	for x := 0; x < n; x++ {
		for y := 0; y <= x; y++ {
			if b.data[x][y] {
				count++
			}
		}
	}

	return count
}

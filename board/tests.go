package board

import (
	"fmt"
)

func RunAllTests() {
	fmt.Println("Running all tests\n")
	TestIsValidJump()
	TestCommitJump()
	TestGetJumpsOf()
	TestStones()
}

func TestIsValidJump() {
	fmt.Println("Running TestIsValidJump")
	n := 4
	b := NewBoard(n, 2, 2)
	jump := Jump{0, 0, 1, 1}
	if !b.isValidJump(jump) {
		panic("Should be a valid jump")
	}
	jump = Jump{0, 0, 1, 0}
	if b.isValidJump(jump) {
		panic("Shouldn't be valid jump")
	}
	jump = Jump{0, 0, -1, 0}
	if b.isValidJump(jump) {
		panic("Shouldn't be valid jump")
	}
	fmt.Println("TestIsValidJump succeeded\n")
}

func TestCommitJump() {
	fmt.Println("Running TestCommitJump")
	b := NewBoard(4, 2, 2)
	x, y, dx, dy := 0, 0, 1, 1
	jump := Jump{x, y, dx, dy}
	b1 := b.commitJump(jump)
	data := b1.data
	if data[x][y] {
		panic("Should have moved stone")
	}
	if data[x+dx][y+dy] {
		panic("Should have removed stone")
	}
	if !data[x+2*dx][y+2*dy] {
		panic("Should have placed stone here")
	}
	if b1.Jumps[0] != "1-6" {
		panic("Jump should be 1-6")
	}
	fmt.Println("TestCommitJump succeeded\n")
}

func TestGetJumpsOf() {
	fmt.Println("Running TestGetJumpsOf")
	b := NewBoard(4, 2, 2)
	b1 := b.getJumpsOf(0, 0)
	if len(b1) != 1 || b1[0].Jumps[0] != "1-6" {
		panic("Jump should be 1-6")
	}
	b2 := b.getJumpsOf(2, 2)
	if len(b2) != 0 {
		panic("There should be no jumps possible")
	}

	b = NewBoard(4, 0, 0)
	b = b.commitJump(Jump{2, 0, -1, 0})
	b = b.commitJump(Jump{2, 2, 0, -1})
	b = b.commitJump(Jump{3, 0, -1, 0})
	b3 := b.getJumpsOf(0, 0)
	if len(b3) != 2 {
		panic("There should be two possible jumps")
	}

	fmt.Println("TestGetJumpsOf succeeded\n")
}

func TestStones() {
	fmt.Println("Running TestStones")
	b := NewBoard(4, 0, 0)
	if b.stones() != 9 {
		panic("There should be 9 stones")
	}
	b = b.commitJump(Jump{2, 0, -1, 0})
	if b.stones() != 8 {
		panic("There should be 8 stones")
	}
	b = b.commitJump(Jump{2, 2, 0, -1})
	if b.stones() != 7 {
		panic("There should be 7 stones")
	}
	b = b.commitJump(Jump{3, 0, -1, 0})
	if b.stones() != 6 {
		panic("There should be 6 stones")
	}
	fmt.Println("TestStones succeeded\n")
}

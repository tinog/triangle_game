package board

import (
	"math/rand"
)

/* Return winning board, true 0 if found winning board
 * Return board, false, undashNumber if not found winning board at leaf
 * ATM uses uniform distribution for both undashing and dashing
 */
func (b Board) DashForWinGame(depth, curDash, maxDash int) (Board, bool, int) {
	if b.stones() == 1 {
		return b, true, 0
	}
	allJumps := b.getAllJumps()
	if len(allJumps) == 0 {
		if depth == 0 {
			return b, false, 0
		}
		return b, false, rand.Intn(depth)
	}
	for {
		choose := rand.Intn(len(allJumps))
		tryGame := allJumps[choose]
		winGame, foundWinGame, undash := tryGame.DashForWinGame(depth+1, curDash, maxDash)
		if foundWinGame {
			return winGame, true, 0
		} else if undash > 0 {
			return b, false, undash - 1
		}
		curDash++
		if curDash == maxDash {
			return b, false, 0
		}
	}
}

func (b Board) GetAWinGame() (Board, bool) {
	if b.stones() == 1 {
		return b, true
	}
	allJumps := b.getAllJumps()
	for _, nextBoard := range allJumps {
		winGame, foundWinGame := nextBoard.GetAWinGame()
		if foundWinGame {
			return winGame, true
		}
	}
	return b, false
}

// Gets all possible win games
func (b Board) GetAllWinGames() []Board {
	if b.stones() == 1 {
		return []Board{b}
	}

	allJumps := b.getAllJumps()
	if len(allJumps) == 0 {
		return make([]Board, 0)
	}

	winGames := make([]Board, 0)
	for _, next_board := range allJumps {
		winGames = append(winGames, next_board.GetAllWinGames()...)
	}
	return winGames
}
